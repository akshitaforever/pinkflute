﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkFlute
{
    public class TypeBroker
    {
        public TypeModel Model { get; set; }
        public Container BaseContainer { get; set; }
        public TypeBroker(TypeModel model,Container basecontainer)
        {
            this.Model = model;
            this.BaseContainer = basecontainer;
        }


    }
}

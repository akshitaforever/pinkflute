﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkFlute
{
    public static class BrokerExtentions
    {
        public static TypeBroker AsSingleton(this TypeBroker broker)
        {
            broker.Model.IsSingleton = true;
            return broker;
        }
        public static TypeBroker AsInstance<T>(this TypeBroker broker, T instance)
        {
            broker.Model.HasInstance = true;
            broker.Model.Instance = instance;
            return broker;
        }
        public static TypeBroker ConstructGeneric(this TypeBroker broker, params Type[] args)
        {
            broker.Model.IsConstructedGeneric = true;
            return broker;
        }
        public static TypeBroker WithParameters(this TypeBroker broker, params Object[] args)
        {
            broker.Model.ConstructorArgs = args.ToList();
            return broker;
        }
        public static TypeBroker WithInternalParameters(this TypeBroker broker, params Type[] args)
        {
            broker.Model.ConstructorArgs = args.Select(s => broker.BaseContainer.Resolve(s)).ToList();
            return broker;
        }
    }
}

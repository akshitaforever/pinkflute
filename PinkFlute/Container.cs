﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PinkFlute
{
    public class Container
    {
        Dictionary<Type, TypeBroker> entries = new Dictionary<Type, TypeBroker>();
        public TypeBroker Register<TBase, TConcrete>() where TConcrete : TBase
        {
            var broker = CreateBroker(new TypeModel() { BaseType = typeof(TBase), ConcreteType = typeof(TConcrete) });
            entries.Add(typeof(TBase), broker);
            return broker;
        }
        public TypeBroker Register(Type baseType, Type ConcreteType)
        {
            var broker = CreateBroker(new TypeModel() { BaseType = baseType, ConcreteType = ConcreteType });
            entries.Add(baseType, broker);
            return broker;
        }
        public TBase Resolve<TBase>() where TBase : class
        {
            var broker = GetBroker(typeof(TBase));
            if (broker != null)
            {
                return broker.Model.GetObject<TBase>() as TBase;
            }
            return null;
        }
        private TypeBroker GetBroker(Type service)
        {
            if (entries.ContainsKey(service))
            {
                return entries[service];
            }
            var entry = (entries.Values.FirstOrDefault(p =>
            {
                if (service.IsConstructedGenericType)
                {
                    var timpl = service.GetGenericTypeDefinition();
                    if (!p.Model.IsConstructedGeneric)
                        return false;
                    var i = p.Model.ConcreteType.GetGenericTypeDefinition().GetInterfaces();
                    var intrfce = p.Model.ConcreteType.GetGenericTypeDefinition().GetInterface(timpl.Name);
                    if (intrfce != null)
                    {
                        return intrfce.GUID == timpl.GUID;
                    }
                    return false;
                }
                return false;
            }));
            return entry;
        }
        public Object Resolve(Type serviceType)
        {
            if (entries.ContainsKey(serviceType))
            {
                return entries[serviceType].Model.GetObject();
            }
            return null;
        }
        private TypeBroker CreateBroker(TypeModel model)
        {
            return new TypeBroker(model, this);
        }
    }
   
  
   
   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PinkFlute
{
    public class TypeModel
    {
        public Type BaseType { get; set; }
        public Type ConcreteType { get; set; }
        public bool IsSingleton { get; set; }
        public Object Instance { get; set; }
        public bool HasInstance { get; set; }
        public bool IsConstructedGeneric { get; set; }
        public List<Object> ConstructorArgs { get; set; }
        public TypeModel()
        {
            ConstructorArgs = new List<object>();
        }
        public Object GetObject()
        {
            if (IsSingleton)
            {
                if (Instance != null)
                    return Instance;
                else
                {
                    Instance = CreateObject();
                    return Instance;
                }
            }
            else
            {
                if (HasInstance)
                    return Instance;
                return CreateObject();
            }
        }
        public T GetObject<T>() where T : class
        {
            if (IsSingleton)
            {
                if (Instance != null)
                    return Instance as T;
                else
                {
                    Instance = CreateObject<T>();
                    return Instance as T;
                }
            }
            else
            {
                if (HasInstance)
                    return Instance as T;
                return CreateObject<T>();
            }
        }
        private Object CreateObject()
        {

            if (IsConstructedGeneric)
                return GetGenericObject();
            return Activator.CreateInstance(ConcreteType,GetConstructorArgs());
        }
        private Object[] GetConstructorArgs()
        {
            //ConstructorArgs = ConstructorArgs ?? new List<Object>();
            //foreach (var item in InternalConstructorArgs)
            //{
            //    ConstructorArgs.Add(GetObject());
            //}
            return ConstructorArgs.ToArray();
        }
        private T CreateObject<T>() where T : class
        {
            if (IsConstructedGeneric)
                return GetGenericObject<T>();
            return Activator.CreateInstance<T>();
        }
        //private  GetObject<TImplementation>() where TImplementation : class
        //{

        //    if (TConcrete.IsConstructedGenericType)
        //    {
        //        if (_generic)
        //        {
        //            //var instance=Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(this.TImplementation.GenericTypeArguments));
        //            var instance = Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(typeof(TImplementation).GenericTypeArguments[0]));
        //            return instance as TImplementation;
        //        }
        //        else
        //            return Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(this.TImplementation.GenericTypeArguments)) as TImplementation;
        //    }
        //    return Activator.CreateInstance(TConcrete) as TImplementation;
        //}

        private Object GetGenericObject()
        {
            
            
            if (ConcreteType.IsConstructedGenericType)
            {
                var instance = Activator.CreateInstance(this.BaseType.GetGenericTypeDefinition().MakeGenericType(ConcreteType.GenericTypeArguments),GetConstructorArgs());
                return instance;
            }
            return Activator.CreateInstance(BaseType);
        }
       
        private T GetGenericObject<T>() where T : class
        {


            var instance = Activator.CreateInstance(this.ConcreteType.GetGenericTypeDefinition().MakeGenericType(typeof(T).GenericTypeArguments),GetConstructorArgs());
            return instance as T;

            //  return Activator.CreateInstance(BaseType) as T;
        }
    }
}

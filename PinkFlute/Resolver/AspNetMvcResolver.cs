﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PinkFlute.Resolver
{
    public class AspNetMvcResolver : IDependencyResolver
    {
        private Container container = null;
        public AspNetMvcResolver(Container container)
        {
            this.container = container;
        }
        public object GetService(Type serviceType)
        {
            if (serviceType.Equals(typeof(IControllerFactory)))
            {
                return new DefaultControllerFactory();
            }

            return this.container.Resolve(serviceType);



        }
        public T GetService<T>() where T : class
        {
            return this.container.Resolve<T>();

        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new Object[] { GetService(serviceType) };
        }
    }
}
